# ng-playlist - (JPlayer, Bootstrap-UI, ui-router, angular-datatable) AngularJS app

### Install

```bash
git clone https://alexmarch@bitbucket.org/alexmarch/ng-playlist.git
```

### Run

```bash
cd app
python -m SimpleHTTPServer
open http://localhost:8000/
```