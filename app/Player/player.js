angular.module('app')
  .controller('PlayerCtrl', ['$scope', function ($scope) {

  }])
  .controller('PlayerInstanceCtrl', ['$scope', '$modalInstance', 'playlists', 'playlist',
    function ($scope, $modalInstance, playlists, playlist) {
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
    $scope.close = function () {
      $modalInstance.close();
    };
    $scope.title =  playlists.data[playlist].name;
    $scope.tracks = playlists.data[playlist].tracks;
  }])
  .service('$player', ['$modal', '$playlist', function ($modal, $playlist) {
    this.open = function (index) {
      var modalInstance = $modal.open({
        templateUrl: 'Player/player.html',
        controller: 'PlayerInstanceCtrl',
        size: 'lg',
        backdrop: true,
        resolve: {
          playlists: function () {
            return $playlist.get();
          },
          playlist: function () {
            return index;
          }
        }
      });
    }
  }]);