'use strict';

angular.module('app')
  .config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
      $urlRouterProvider.otherwise("/app/home");
      $stateProvider
        .state('app', {
          url: '/app',
          templateUrl: 'CouchMusic/app.html',
          controller: 'CouchMusicCtrl'
        })
        .state('app.home', {
          url: '/home',
          views: {
            "content@app": {
              templateUrl: 'CouchMusic/home.html',
              controller: 'CouchMusicCtrl'
            }
          }
        })
        .state('app.playlists', {
          url: '/playlists',
          views: {
            "content@app": {
              templateUrl: 'Playlist/playlist.html',
              controller: 'PlayListCtrl'
            }
          }
        });
    }]);
