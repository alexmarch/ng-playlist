'use strict';

var app = angular.module('app', [
  'ngRoute',
  'ui.router',
  'datatables',
  'ui.bootstrap',
  'app.playlist',
  'jplayer'
]);