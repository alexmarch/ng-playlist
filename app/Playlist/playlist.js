angular.module('app')
  .controller('PlayListCtrl', ['$scope', 'DTOptionsBuilder', 'DTColumnBuilder', '$filter', '$player', '$compile',
    function ($scope, DTOptionsBuilder, DTColumnBuilder, $filter, $player, $compile) {

      $scope.open = function( index ){
        $player.open( index );
      };

      $scope.dtOptions = DTOptionsBuilder.fromSource('Playlist/playlists.json')
        .withPaginationType('simple_numbers')
        .withOption('bFilter', false)
        .withOption('bInfo', false)
        .withBootstrap()
        .withOption('fnRowCallback',
        function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
          $compile(nRow)($scope);
        });

      $scope.dtColumns = [
        DTColumnBuilder.newColumn('tracks').withClass('sorting_disabled text-center').withTitle('Listen').renderWith( function(data, type, full, meta) {
          return '<button type="button" class="btn btn-primary btn-xs" ng-click="open('+ meta.row +')"><i class="fa fa-play"></i></button>';
        }),
        DTColumnBuilder.newColumn('name').withTitle('Playlist Name').renderWith( function( data, type, full) {
          return '<a href="#">' + data + '</a>';
        }),
        DTColumnBuilder.newColumn('updated').withTitle('Updated').withClass('text-center').renderWith( function( data, type, full) {
          return $filter('date')(data, 'yyyy-MM-dd HH:mm:ss');
        }),
        DTColumnBuilder.newColumn('tracks').withTitle('#Tracks').withClass('text-center').renderWith( function( data, type, full) {
          if (angular.isArray(data)) {
            return data.length;
          }
          return 0;
        }),
        DTColumnBuilder.newColumn('visibility').withTitle('Shared').withClass('text-center').renderWith( function(data, type, full) {
          if (data == "PUBLIC") {
            return "Y";
          }
          return "N"
        })
      ];
    }]);