angular.module('jplayer', [])
  .directive('jPlayer', function () {
    return {
      restrict: 'AE',
      replace: true,
      templateUrl: 'components/jplayer.html',
      scope: {
        tracks: "="
      },
      link: function ($scope, element, attrs) {
        this.myPlaylist = new jPlayerPlaylist({
          jPlayer: "#jquery_jplayer_1",
          cssSelectorAncestor: "#jp_container_1"
        }, $scope.tracks, {
          playlistOptions: {
            enableRemoveControls: true
          },
          ready: function(event) {
            console.log("ready");
          },
          muted: false,
          swfPath: "../../dist/jplayer",
          supplied: "mp3",
          useStateClassSkin: true,
          autoBlur: false,
          smoothPlayBar: true,
          keyEnabled: true
        });
      }
    }
  });