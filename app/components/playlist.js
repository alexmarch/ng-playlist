angular.module('app.playlist',[])
  .service('$playlist',['$http', function($http) {
    this.get = function() {
      return $http.get('Playlist/playlists.json')
    }

  }]);