angular.module('app')
  .controller('CouchMusicCtrl', ['$scope', '$modal', function ($scope, $modal) {
    $scope.user = {
      name: 'johndoe',
      fullName: 'John Doe'
    };
    $scope.about = function() {
      $modal.open({
        templateUrl: 'CouchMusic/about.html',
        controller: ['$scope', '$modalInstance', function ($scope, $modalInstance) {
          $scope.close = function(){
            $modalInstance.close();
          }
        }],
        size: 'lg',
        backdrop: true
      });
    }
  }])
