'use strict';

var gulp = require('gulp');

var bower_componetns_path = 'app/bower_components';

gulp.task('copy', function () {
  gulp.src([
    bower_componetns_path + '/**/*.js',
    bower_componetns_path + '/**/*.css',
    bower_componetns_path + '/**/*.map',
    bower_componetns_path + '/**/*.png',
    bower_componetns_path + '/**/fonts/**'
  ]).pipe(gulp.dest('app/vendor'))

});
